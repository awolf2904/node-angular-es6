// index.js
import Hapi from 'hapi';
import staticRoutes from './static.routes';
import apiRoutes from './api.routes';

var server = new Hapi.Server();

server.connection({ port: 3000 });

server.start(function () {
    console.log('Server running at:', server.info.uri);
});

server.route(staticRoutes);
server.route(apiRoutes);