// app.js

import 'angular';

class HomeController {
	constructor() {
		this.hello = 'Hello world from ES6 HomeController.';
	}
}

HomeController.$inject = [];


export default angular.module('myApp', [])
	.controller('homeController', HomeController);