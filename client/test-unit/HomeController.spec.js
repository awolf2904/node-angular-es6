// HomeController.spec.js

import 'app/app';
import 'angular-mocks';

describe('Test suite for HomeController', function() {

	// var homeController;

	beforeEach(function() {
		module('myApp');
		
		// inject(function($controller) {
		// 	console.log('inject called', $controller);
		// 	//controller = $controller;	
		// 	homeController = $controller('homeController');
		// });
	});

	it('should say "Hello World"', inject(function($controller) {

			var homeController = $controller('homeController');

			// console.log(homeController, angular.mocks);
			expect(homeController.hello).toEqual('Hello world from ES6 HomeController.'); // will not fail
		})
	);
});