## What is this repository for? ##

This demo app is for creating new projects with a node server and angular front-end app.
With the following features:

* node server with Hapi (in progress)
* Browsersync as development server
* Babel for ES6 transpiling (working)
* AngularJS 1.4x (simple hello world added)
* SASS compilation (working)
* Karma test runner (working)
* JSPM package manager (working)
* No built taskrunner (Grunt/Gulp) beside NPM.

## Why not take one of the many other seed projects? ##
I've read this [blog post](http://blog.keithcirkel.co.uk/why-we-should-stop-using-grunt/) and I think he's absolutely right. I first wanted to start with gulp but I've always thought it's pretty complicated for newbies like me.

## How do I get set up? ##

### Summary of set up ###
In root directory run `npm install`.
`jspm install` will run in postinstall of npm to automate this.

For testing please call `jspm install --dev` aditionally.

### How to run the app ###
In root direcotry run `npm start`. At the moment this simply serves `client/app` folder with `npm-serve`. 
Later this will be done by a node server.

After running `npm run build` you can run `npm run serve` which will serve the dist folder with node server and proxy to browsersync.

### Dependencies
- Angular
- NodeJs

- Karma
- Babel

### How to run tests
`npm test` to run unit test for angular js app.

## Who do I talk to? ##

* Repo owner A. Wolf / AWolf2904@gmail.com

## Todo ##
- Add protractor E2E tests for angular
- Setup tool chain build `npm run build` + minification + distribution --> implemented, index.jade could be improved by importing the body of index.hmtl
- Watch:build not working yet (WIP)
- Check dependencies and what is not required
- Setup node server (Express / COA or other framework --> google what's the best solution) --> I'll try hapi
- Create a better angular demo app (e.g. a todo app)